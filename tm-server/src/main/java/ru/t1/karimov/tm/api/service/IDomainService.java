package ru.t1.karimov.tm.api.service;

import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {

    void dropDatabaseScheme() throws DatabaseException;

    @NotNull
    String getBackupFile();

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataYamlFasterXml();

    void updateDatabaseScheme() throws LiquibaseException;
}
