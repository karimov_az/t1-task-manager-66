package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.api.service.dto.*;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.endpoint.AbstractEndpoint;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PID_FILENAME = "task-manager.pid";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initDemoData() throws Exception {
        @NotNull final String demoLoad = propertyService.getDataDemoLoad();
        if ("n".equals(demoLoad)) return;

        if (userService.findByLogin("test") == null) {
            userService.create("test", "test", "test@test.ru");
        }
        if (userService.findByLogin("user") == null) {
            userService.create("user", "user", "user@user.ru");
        }
        if (userService.findByLogin("admin") == null) {
            userService.create("admin","admin", Role.ADMIN);
        }

        @Nullable final UserDto user1 = userService.findByLogin("test");
        @Nullable final UserDto user2 = userService.findByLogin("user");
        @Nullable final UserDto user3 = userService.findByLogin("admin");
        if (user1 == null) throw new UserNotFoundException();
        if (user2 == null) throw new UserNotFoundException();
        if (user3 == null) throw new UserNotFoundException();
        @NotNull final String user1_id = user1.getId();
        @NotNull final String user2_id = user2.getId();
        @NotNull final String user3_id = user3.getId();

        projectService.add(new ProjectDto(user1_id,"Project_01", "Desc_01_user 1", Status.IN_PROGRESS));
        projectService.add(new ProjectDto(user1_id, "Project_02", "Desc_02_user 1", Status.NOT_STARTED));
        projectService.add(new ProjectDto(user1_id, "Project_03", "Desc_03_user 1", Status.IN_PROGRESS));
        projectService.add(new ProjectDto(user1_id, "Project_04", "Desc_04_user 1", Status.COMPLETED));
        projectService.add(new ProjectDto(user2_id,"Project_01", "Desc_01_user 2", Status.IN_PROGRESS));
        projectService.add(new ProjectDto(user2_id, "Project_02", "Desc_02_user 2", Status.NOT_STARTED));
        projectService.add(new ProjectDto(user3_id, "Project_01", "Desc_01_user 3", Status.IN_PROGRESS));
        projectService.add(new ProjectDto(user3_id, "Project_02", "Desc_02_user 3", Status.COMPLETED));

        final String projectId1 = projectService.findOneByIndex(user1_id,3).getId();
        final String projectId2 = projectService.findOneByIndex(user2_id,1).getId();
        final String projectId3 = projectService.findOneByIndex(user3_id,1).getId();

        taskService.add(new TaskDto(user1_id, "Task_01", "Desc task 1 user 1", Status.IN_PROGRESS, null));
        taskService.add(new TaskDto(user1_id, "Task_02", "Desc task 2 user 1", Status.NOT_STARTED, null));
        taskService.add(new TaskDto(user1_id, "Task_03", "Desc task 3 user 1", Status.COMPLETED, projectId1));
        taskService.add(new TaskDto(user1_id, "Task_04", "Desc task 4 user 1", Status.NOT_STARTED, projectId1));
        taskService.add(new TaskDto(user2_id, "Task_01", "Desc task 1 user 2", Status.IN_PROGRESS, projectId2));
        taskService.add(new TaskDto(user2_id, "Task_02", "Desc task 2 user 2", Status.NOT_STARTED, null));
        taskService.add(new TaskDto(user3_id, "Task_01", "Desc task 1 user 3", Status.COMPLETED, projectId3));
        taskService.add(new TaskDto(user3_id, "Task_01", "Desc task 2 user 3", Status.NOT_STARTED, null));
    }

    @SneakyThrows
    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    public void start() throws Exception {
        initEndpoints();
        initPID();
        loggerService.initJmsLogger();
        initDemoData();
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

}
