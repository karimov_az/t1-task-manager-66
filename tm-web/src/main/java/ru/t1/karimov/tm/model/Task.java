package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.karimov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column
    private String description;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_start")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
