package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.model.Project;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    private static final String ID_EMPTY = "Id is empty";

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.save(model);
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException(ID_EMPTY);
        projectRepository.deleteById(id);
    }

    @Nullable
    public Project findOneById(@Nullable final String id) {
        if (id == null) throw new EntityNotFoundException(ID_EMPTY);
        return projectRepository.findById(id).orElse(null);
    }

    @NotNull
    public String getProjectName(@Nullable final String id) {
        @Nullable final Project project = findOneById(id);
        if (project == null) return "";
        @NotNull final String name = project.getName();
        return name;
    }

}
