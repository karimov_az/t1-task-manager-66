package ru.t1.karimov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.model.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
}
